<?php

namespace App\Http\Controllers;

use App\DemoUser;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class DemoUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('display');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable(Request $request)
    {
        $getdata =  DemoUser::get();
        return Datatables::of($getdata)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DemoUser  $demoUser
     * @return \Illuminate\Http\Response
     */
    public function show(DemoUser $demoUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DemoUser  $demoUser
     * @return \Illuminate\Http\Response
     */
    public function edit(DemoUser $demoUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DemoUser  $demoUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DemoUser $demoUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DemoUser  $demoUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(DemoUser $demoUser)
    {
        //
    }
}
