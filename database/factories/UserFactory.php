<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DemoUser;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\DemoUser::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'intro' => $faker->paragraph($nb = 3, $asText = false),
        'dateOfBirth' => $faker->date($format = 'Y-m-d', $max = 'now'),
    ];
});