@extends('layouts.default')
@section("content")
	<div class="row">
        <div class="col-lg-12 my-5">
            <h3>User Data</h3>
            <button class="btn btn-primary" id="refreshDataTable">Refresh Data</button>
        </div>
        <div class="col-lg-4" id="info-wrapper">
            <h2>User info</h2>
            <div class="card">
                <div class="card-body">
                <h4 class="card-title" id="name"></h4>
                <h5 class="card-title"><b>Email:</b> <span id="email"></span> </h5>
                <h5 class="card-title"><b>Date Of Birth:</b> <span id="dateOfBirth"></span> </h5>
                <p class="card-text" id="profile-info"></p>
                </div>
            </div>
        </div>
		<div class="col-lg-8 offset-lg-4" id="table-wrapper">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="userDatatable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        
	</div>
@endsection
@push('script')
<script>
	var dtable;
    $(document).ready(function() {
        $('#info-wrapper').hide();
        dtable = $('#userDatatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: 'api/datatables',
            },
            order: [[ 4, "desc" ]],
            columns: [
                {
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'email',
                    name: 'email'
                },              
                {
                    data: 'created_at',
                    name: 'created_at'
                },
                {
                    data: 'updated_at',
                    name: 'updated_at'
                },
            ]
        });
        $("#refreshDataTable").click(function(e) {
            dtable.draw();
            $('#table-wrapper').addClass('offset-lg-4'); 
            $('#info-wrapper').hide();
            $('#name').text('');
            $('#email').text('');
            $('#dateOfBirth').text('');
            $('#profile-info').text('');
        });
        $('#userDatatable tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected'); 
                $('#table-wrapper').addClass('offset-lg-4'); 
                $('#info-wrapper').hide();
                $('#name').text('');
                $('#email').text('');
                $('#dateOfBirth').text('');
                $('#profile-info').text('');
            }
            else {
                dtable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                $('#table-wrapper').removeClass('offset-lg-4'); 
                var data = $('#userDatatable').DataTable().row('.selected').data();
                $('#name').text(data.name);
                $('#email').text(data.email);
                var m = new Date(data.dateOfBirth);
                var dateString = m.getUTCFullYear() +"/"+ (m.getUTCMonth()+1) +"/"+ m.getUTCDate();
                $('#dateOfBirth').text(dateString);
                $('#profile-info').text(data.intro);
                $('#info-wrapper').show();
            }
        });
    });

</script>
@endpush